import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.ukui.quick.widgets 1.0
import org.ukui.quick.items 1.0

WidgetItem {
    property bool panelOrientation: (Widget.orientation === Types.Horizontal)
    property int pos: Widget.container.position
    property real ratio: parent.ratio ? parent.ratio : 1
    Layout.fillWidth: panelOrientation ? false : true
    Layout.fillHeight: panelOrientation ? true : false
    Layout.preferredWidth: childrenRect.width
    Layout.preferredHeight: childrenRect.height
    clip: true
    Widget.active: dropArea.panelActive

    TrayView {
        id: dropArea
        property bool panelActive: false
        property bool onTopBar: parent.height < 40
        scaleFactor: parent.ratio * 8
        itemOrientation: parent.panelOrientation
        itemWidth: onTopBar ? 28 : parent.panelOrientation ?
                                  scaleFactor * 4 : scaleFactor * 5
        itemHeight: onTopBar ? 28 : parent.panelOrientation ?
                                   scaleFactor * 5 : scaleFactor * 4
        itemLong: onTopBar ? 28 : scaleFactor * 5
        itemShort: onTopBar ? 28 : scaleFactor * 4
        panelPos: parent.pos
    }

    onWidthChanged: {
        dropArea.changeFoldWindowPosition();
    }

    onHeightChanged: {
        dropArea.changeFoldWindowPosition();
    }
}
